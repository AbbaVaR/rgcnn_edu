import pathlib
from dataclasses import dataclass
from torch_geometric.transforms import (
    Compose,
    RandomRotate,
    FixedPoints,
    NormalizeScale,
)


dataset_path = pathlib.Path(__file__).parent.resolve() / 'dataset'
log_dir = pathlib.Path(__file__).parent.resolve() / 'logdir'
save_dir = pathlib.Path(__file__).parent.resolve() / 'trained'
init_weight = pathlib.Path(__file__).parent.resolve() / 'rgcnn_pytorch/init_weights/2048p_model_v2_15.pt'

categories = ['Table', 'Chair', 'Airplane', 'Lamp', 'Car', 'Guitar']
batch_size = 25
num_epochs = 200
learning_rate = 1e-4

num_points = 2048
transforms = Compose([
    FixedPoints(num_points),
    NormalizeScale(),
    RandomRotate(180, 0),
    RandomRotate(180, 1),
    RandomRotate(180, 2)
])
model_config = dict(
    vertice=num_points, 
    F = [128, 512, 1024],  # Outputs size of convolutional filter.
    K = [6, 5, 3],         # Polynomial orders.
    M = [512, 128, 50],
    input_dim = 22,
    dropout = 0.15,
    one_layer=False,
    reg_prior=True,
    recompute_L=False,
    b2relu=True
)