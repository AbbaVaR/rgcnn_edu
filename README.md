# rgcnn_pytorch

## Источники
- Статья о модели: https://arxiv.org/abs/1806.02952
- Источник модели: https://github.com/DomsaVictor/rgcnn_pytorch
- Обучение адаптировано под датасет ShapeNet: https://pytorch-geometric.readthedocs.io/en/latest/generated/torch_geometric.datasets.ShapeNet.html#torch_geometric.datasets.ShapeNet
