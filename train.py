import os
import sys

import config
from rgcnn_pytorch.train_shapenet import train_shapenet


def start_train():
    train_shapenet(dataset_path=config.dataset_path, categories=config.categories, transforms=config.transforms, 
                   model_config=config.model_config ,log_dir=config.log_dir, save_dir=config.save_dir, init_weight=config.init_weight, 
                   batch_size=config.batch_size, num_epochs=config.num_epochs, learning_rate=config.learning_rate)

if __name__ == '__main__':
    start_train()
